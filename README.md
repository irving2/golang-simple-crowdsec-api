## DO
- change `shellscriptpath` variable to fit your environment see line 33 `src/service.go`. Make sure to edit `shell.sh` in `/src/`
- Add whitelist IPs in `src/whitelist.txt`
- Update environment variables `.env-prod` (for production) and `.env-dev` (for local development)
- Install Crowdsec `https://crowdsec.net/`

## RUN/BUILD on DEVELOPMENT 
```
export APP_ENV=dev 
go run src/service.go
```
```
export APP_ENV=dev 
go build src/service.go
```

## RUN/BUILD on PROD 
```
export APP_ENV=prod 
go run src/service.go
```
```
export APP_ENV=prod
go build src/service.go
```

## ADD IP
curl --request POST 'http://localhost:3030/crowdsec/1.2.3.9' -u "abc:123"

## DELETE IP 
curl --request POST 'http://localhost:3030/crowdsec/1.2.3.9' -u "abc:123"

## QUERY IP
curl --request POST 'http://localhost:3030/crowdsec/1.2.3.9' -u "abc:123"


## RUN AS SERVICE

Update the service template file based on your environment and copy it to `/lib/systemd/system/` as `service-api-crowdsec.service`

```
/lib/systemd/system/service-api-crowdsec.service
```

Your system service has been added. Let’s reload the systemctl daemon to read new file. You need to reload this deamon each time after making any changes in in .service file.

```
systemctl daemon-reload 
systemctl enable service-api-crowdsec.service
```