package main

//cscli decisions add -i <ip>
//cscli decisions delete -i <ip>

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"os/exec"
	"regexp"

	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
)

var (
	user            string
	pass            string
	shellscriptpath string
	whitelistfile   string
	shellscript     string
	envfile         string
	environment     string
)

type Message struct {
	ip string
}

func main() {
	user = goDotEnvVariable("user")
	pass = goDotEnvVariable("pass")
	shellscriptpath = goDotEnvVariable("shellscriptpath")
	whitelistfile = goDotEnvVariable("whitelistfile")
	shellscript = goDotEnvVariable("shellscript")

	r := mux.NewRouter()
	r.HandleFunc("/crowdsec/{ipaddress}", crowdsec)
	http.ListenAndServe(":3030", r)
}

func goDotEnvVariable(key string) string {

	environment = os.Getenv("APP_ENV")

	fmt.Println(environment)

	if environment == "prod" {
		envfile = ".env-prod"
	} else {
		envfile = ".env-dev"
	}

	err := godotenv.Load(envfile)

	if err != nil {
		log.Fatalf("Error loading .env file")
	}

	return os.Getenv(key)
}

func checkifipexists(ipaddress string, body string) bool {

	isExist, err := regexp.MatchString(`\b`+ipaddress+`\b`, body)
	if err != nil {
		panic(err)
	}
	return isExist
}

func checkwhitelist(ipaddress string) bool {
	b, err := ioutil.ReadFile(whitelistfile)
	if err != nil {
		panic(err)
	}
	isExist, err := regexp.Match(`\b`+ipaddress+`\b`, b)
	if err != nil {
		panic(err)
	}
	return isExist
}

func checkifvalidip(ip string) bool {
	if net.ParseIP(ip) == nil {
		return false
	} else {
		return true
	}
}

func crowdsec(w http.ResponseWriter, r *http.Request) {

	u, p, ok := r.BasicAuth()
	if !ok {
		fmt.Println("Error parsing basic auth")
		w.WriteHeader(401)
		return
	}
	if u != user {
		fmt.Printf("invalid user: %s\n", u)
		w.WriteHeader(401)
		return
	}
	if p != pass {
		fmt.Printf("invalid pass: %s\n", p)
		w.WriteHeader(401)
		return
	}

	vars := mux.Vars(r)
	ipaddress, ok := vars["ipaddress"]
	if !ok {
		fmt.Println(ipaddress)
	}

	if !checkifvalidip(ipaddress) {

		resp := make(map[string]string)
		resp["message"] = ipaddress + " is an invalid IP"
		jsonResp, err := json.Marshal(resp)
		if err != nil {
			log.Fatalf("Error : %s", err)
		}
		w.Write(jsonResp)
		return

	} else {

		if !checkwhitelist(ipaddress) {
			switch r.Method {

			case "GET":
				cmd := exec.Command(shellscriptpath+shellscript, "list", ipaddress)
				stdout, err := cmd.Output()
				if err != nil {
					fmt.Println(err.Error())
					return
				}
				w.Write(stdout)
				return

			case "POST":

				//SIMULATE : cscli decisions add -i <ipaddress>
				cmd := exec.Command(shellscriptpath+shellscript, "list", ipaddress)
				stdout, err := cmd.Output()
				if err != nil {
					fmt.Println(err.Error())
					return
				}
				if checkifipexists(ipaddress, string(stdout)) {
					resp := make(map[string]string)
					resp["message"] = ipaddress + " already blocked"
					jsonResp, err := json.Marshal(resp)
					if err != nil {
						log.Fatalf("Error : %s", err)
					}
					w.Write(jsonResp)
				} else {
					cmd := exec.Command(shellscriptpath+shellscript, "add", ipaddress)
					stdout, err := cmd.Output()
					if err != nil {
						fmt.Println(err.Error())
						return
					}
					fmt.Println(string(stdout))

					resp := make(map[string]string)
					resp["message"] = ipaddress + " added"
					jsonResp, err := json.Marshal(resp)
					if err != nil {
						log.Fatalf("Error : %s", err)
					}
					w.Write(jsonResp)
				}

				return

			case "DELETE":

				//SIMULATE : cscli decisions delete -i <ipaddress>
				cmd := exec.Command(shellscriptpath+shellscript, "delete", ipaddress)
				stdout, err := cmd.Output()
				if err != nil {
					fmt.Println(err.Error())
					return
				}
				fmt.Println(string(stdout))
				resp := make(map[string]string)
				resp["message"] = ipaddress + " deleted"
				jsonResp, err := json.Marshal(resp)
				if err != nil {
					log.Fatalf("Error : %s", err)
				}
				w.Write(jsonResp)
				return

			default:
				fmt.Print("POST, DELETE only")
			}
		} else {
			resp := make(map[string]string)
			resp["message"] = ipaddress + " is whitelisted"
			jsonResp, err := json.Marshal(resp)
			if err != nil {
				log.Fatalf("Error : %s", err)
			}
			w.Write(jsonResp)
			return
		}

	}

}
