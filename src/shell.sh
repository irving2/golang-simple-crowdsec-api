#!/bin/sh

csclicommand=$1
ipaddress=$2
if [ "$csclicommand" == "add" ]; then
	check=$(cscli decisions list -i "$ipaddress")
	if  echo "$check" | grep -q "$ipaddress" 
	then
		echo "already blocked"
	else
		cscli decisions "$csclicommand" -i "$ipaddress"
	fi
else
    cscli decisions "$csclicommand" -i "$ipaddress"
fi
