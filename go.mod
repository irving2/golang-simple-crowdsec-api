module crowdsec-golang-api

go 1.16

require (
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/joho/godotenv v1.4.0 // indirect
	github.com/labstack/echo/v4 v4.6.3 // indirect
)
